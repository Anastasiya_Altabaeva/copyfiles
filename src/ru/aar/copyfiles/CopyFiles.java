package ru.aar.copyfiles;

import java.util.Scanner;

/**
 * Класс, реализующий выполнение сначала последовательного копирования,
 * а затем параллельного копирования двух файлов с помощью многопоточности
 *
 * @authors Горбачева, Алтабаева, 16ИТ18к
 */
public class CopyFiles {
    private static final String INFO_ABOUT_COPY = "Файл, расположенный по адресу %s , был успешно скопирован " +
            "в файл, расположенный по адресу %n %s , за %d миллисекунд %n";
    private static final String ENTER_PATH_FROM = "Введите полное имя файла, который нужно скопировать";
    private static final String ENTER_PATH_INTO = "Введите полное имя файла, в который нужно скопировать исходник";
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws InterruptedException {
        System.out.println(ENTER_PATH_FROM);
        /*
         * Полное имя первого исходного файла
         */
        String source1 = scanner.nextLine();
        System.out.println(ENTER_PATH_INTO);
        /*
         * Полное имя первого конечного файла
         */
        String target1 = scanner.nextLine();
        System.out.println(ENTER_PATH_FROM);
        /*
         * Полное имя второго исходного файла
         */
        String source2 = scanner.nextLine();
        System.out.println(ENTER_PATH_INTO);
        /*
         * Полное имя второго конечного файла
         */
        String target2 = scanner.nextLine();
        /*
         * Первый поток для последовательного копирования
         */
        ThreadCopy inSeriesCopy1 = new ThreadCopy(source1, target1);
        /*
         * Второй поток для последовательного копирования
         */
        ThreadCopy inSeriesCopy2 = new ThreadCopy(source2, target2);
        long before1 = System.currentTimeMillis();
        inSeriesCopy1.start();
        inSeriesCopy1.join();
        long after1 = System.currentTimeMillis();
        System.out.printf(INFO_ABOUT_COPY, source1, target1, after1 - before1);
        long before2 = System.currentTimeMillis();
        inSeriesCopy2.start();
        inSeriesCopy2.join();
        long after2 = System.currentTimeMillis();
        System.out.printf(INFO_ABOUT_COPY, source2, target2, after2 - before2);
        System.out.printf("Общее время выполнения копирования %d миллисекунд %n", after2 - before1);
        /*
         * Первый поток для параллельного копирования
         */
        ThreadCopy parallelCopy1 = new ThreadCopy(source1, target1);
        /*
         * Второй поток для параллельного копирования
         */
        ThreadCopy parallelCopy2 = new ThreadCopy(source2, target2);
        before1 = System.currentTimeMillis();
        parallelCopy1.start();
        before2 = System.currentTimeMillis();
        parallelCopy2.start();
        parallelCopy1.join();
        after1 = System.currentTimeMillis();
        parallelCopy2.join();
        after2 = System.currentTimeMillis();
        System.out.printf(INFO_ABOUT_COPY, source1, target1, after1 - before1);
        System.out.printf(INFO_ABOUT_COPY, source2, target2, after2 - before2);
        System.out.printf("Общее время выполнения копирования %d миллисекунд", after2 - before1);
    }
}

/*
D:\work\copyfiles\src\ru\aar\copyfiles\textFrom1.txt
D:\work\copyfiles\src\ru\aar\copyfiles\textInto1.txt
D:\work\copyfiles\src\ru\aar\copyfiles\textFrom2.txt
D:\work\copyfiles\src\ru\aar\copyfiles\textInto2.txt
 */